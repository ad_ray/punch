import subprocess
import multiprocessing
import config
from punchlog import log
queue = None

class Player:
    _player_proc = None
    _queue       = None
    def _play(self, in_queue):
        while True:
            try:
                msg = in_queue.get()
                if msg == 'kill':
                    break
                if msg:
                    log(msg)
                stream = open('/dev/null')
                subprocess.Popen(
                    ['aplay', msg],
                    stdout=stream,
                    stderr=stream,
                    stdin=stream)

            except Exception:
                print('subprocess exception')
    
    def press_sound(self, *args, **kvargs):
        log('press')
        self._queue.put(config.conf_dir + '/press.wav')

    def err_sound(self):
        log('sound')
        self._queue.put(config.conf_dir + '/beep.wav')
    
    def __init__(self):
        self._queue = multiprocessing.JoinableQueue()
     
        self._player_proc = multiprocessing.Process(
            target=lambda: self._play(self._queue))
        self._player_proc.daemon = True
        self._player_proc.start()

    def stop(self):
        self._queue.put('kill')

    def __del__(self):
        self.stop()
