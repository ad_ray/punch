class Event:
  def __init__(self):
    self.handlers = []

  def __add__(self, handler):
    self.handlers.append(handler)
    return self

  def __rsub__(self, handler):
    if handler in self.handlers:
      self.handlers.remove(handler)
    return self
  
  def clean(self):
    self.handlers = []

  def fire(self, *args, **kargs):
    for handler in self.handlers:
      handler(*args, **kargs)

if __name__ == '__main__':
  def h0():
    print('parameterless handler')

  def h1(a):
    print('handler with positional parameter', a)
    
  def h2(b='b'):
    print('handler with named parameter', b)
    

  e0 = Event()
  e1 = Event()
  e2 = Event()

  
  e0 += h0
  e1 += h1
  e2 += h2
  

  e0.fire()
  e1.fire('hell')
  e2.fire(b = 'bb')
