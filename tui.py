import urwid
import logging
from urwid.raw_display import Screen
LOG = 'punch.log'

logging.basicConfig(filename=LOG, level= logging.DEBUG)
logging.debug('log')
palette = [
    ('txtLeft', 'black', 'dark gray','standout'),
    ('txtRight', 'black', 'light gray', 'standout'),
    ('bg', 'black', 'dark blue'),]

class TypingScreen:
  loop     = None
  title    = ''
  
  txtLeft  = urwid.Text(('txtLeft', 'Hell'), align='right')
  txtRight = urwid.Text(('txtRight', 'Yeah'), align='left')
  mapLeft  = urwid.AttrMap(txtLeft, 'txtLeft')
  mapRight = urwid.AttrMap(txtRight, 'txtRight')
  columns  = urwid.Columns([txtLeft, txtRight], min_width=15)
  data     = urwid.Text('go!!!\n' * 6, align='center')
  message  = urwid.Text('\n' * 6, align='center')
  rows = urwid.Pile([data, columns, message])
  fill     = urwid.Filler(rows, 'middle')
  mapBG    = urwid.AttrMap(fill, 'bg')
  inputHandler = None
  getStrings   = None

  def infoChangedHandler(timePast = '00:00',
                         speed    = '0 CPM',
                         errors   = '00.0 %'):
    TypingScreen.data.set_text(
      '{0} / {1} / {2}'.format(timePast, speed, errors) + '\n' * 6)
    TypingScreen.loop.draw_screen()


  def stringsChangedHandler(strings=('','')):
    TypingScreen.setScreenStrings(*strings)

  def pauseChangedHandler(paused=None):
    pass

  def exitHandler():
   TypingScreen.setScreenStrings('stop ', ' please')
   TypingScreen.loop.draw_screen()
   
  def setScreenStrings(txtLeft,txtRight):
    txtLen = 20
    txtLeft = '  ' + txtLeft[-txtLen:]
    txtLeft = ' ' * (txtLen - len(txtLeft)) + txtLeft
    txtRight = txtRight[:txtLen] + '  '
    TypingScreen.txtLeft.set_text(('txtLeft', txtLeft))
    TypingScreen.txtRight.set_text(('txtRight', txtRight))

  def get_input(input):
    TypingScreen.inputHandler(input)

  def show(inputHandler=None):
    TypingScreen.inputHandler = inputHandler
    TypingScreen.loop = urwid.MainLoop(TypingScreen.mapBG, 
                          palette, 
                          unhandled_input=TypingScreen.get_input)
    TypingScreen.loop.run()
    



