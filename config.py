import sys
import os
import configparser
import argparse

from punchlog import log
#from file_ops impors show_stats

conf_dir = os.path.expanduser('~/.punch/')
conf_file = conf_dir + 'punch.conf'

conf = {
    'mode'           : 'phrases',
    'file'           : str(conf_dir) + 'phrases/en',
    'stat_path'      : str(conf_dir) + 'stat/',
    'stat_dir'       : 'en/',
    'stat_time'      : '120',
    'stats'          : False,
    'left_bg_color'  : 'BLUE',
    'right_bg_color' : 'WHITE',
    'left_fg_color'  : 'WHITE',
    'right_fg_color' : 'BLUE',
    'log_level'      : 'DEBUG'
    }


def set_env():
    pass

def set_default_conf():
    conf['mode']           = 'phrases'
    conf['file']           = conf_dir + 'en/en'
    conf['left_bg_color']  = 'BLUE'
    conf['right_bg_color'] = 'WHITE'
    conf['left_fg_color']  = 'WHITE'
    conf['right_fg_color'] = 'BLUE'
    conf['log_level']      = 'DEBUG'

def try_set_option(section, option, cp):
    if (cp.has_section(section) 
        and cp.has_option(section, option)):
        if option == 'stat_time':
            conf[option] = cp.getint(section, option)
        else:
            conf[option] = cp.get(section, option)

def set_conf_from_file():
    cp = configparser.ConfigParser()
    cp.read(conf_file)
    for key in conf.keys():
        try_set_option('conf', key, cp) #options from conf section
    

def set_conf_from_args():
    ap = argparse.ArgumentParser()
    modes = ap.add_mutually_exclusive_group() 
    modes.add_argument('-d', '--digits', action='store_const', 
                       const='digits', dest='mode')
    modes.add_argument('-p', '--phrases', action='store_const',
                       const='phrases', dest='mode')
    ap.add_argument(action='store', type=str, 
                    nargs='?', metavar='FILE', dest='file')
    ap.add_argument('-s', '--stats', action='store_true', 
                    dest='stats')
    args = vars(ap.parse_args())
    if args.get('mode'):
        conf['mode'] = args['mode']
    if args.get('file'):
        conf['file'] = os.path.expanduser(args['file'])
    

def get_config():
    log(sys.argv)
    set_conf_from_file()
    set_conf_from_args()
    if not conf['mode'] in ('phrases', 'book', 'custom'):
        conf['stat_dir'] = conf['mode'] + '/'
    return conf
