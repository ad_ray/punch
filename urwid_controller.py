import tui
import model.roller as roller         

def inputHandler(input):
  if input == 'esc':
    roller.Roller._pause()
  elif not roller.Roller.paused:
    roller.Roller._check_input_n_roll_strings(input)
    
roller.Roller.refreshInfo    += (
  tui.TypingScreen.infoChangedHandler)

roller.Roller.stringsChanged += (
  tui.TypingScreen.stringsChangedHandler)

roller.Roller.pauseChanged   += (
  tui.TypingScreen.pauseChangedHandler)

roller.Roller.exitEvent      += (
  tui.TypingScreen.exitHandler)


try:
  roller.Roller.start()
  tui.TypingScreen.show(inputHandler=inputHandler)
except KeyboardInterrupt:
  roller.Roller.exit()

