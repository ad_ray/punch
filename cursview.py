import curses
import sys

from punchlog import log
from utf_input import get_input


class Screen:

    def __init__(self):
        self.stdscr = None
        self.left_color  = None
        self.right_color = None
        self.term_width  = 84
        self.term_height = 40
        self.extern_input_handler = None
        self.txtLeft  = None
        self.txtRight = None
        self.info     = 'goo!!!'

    
    def print_strings(self):
        txtLeft  = self.txtLeft
        txtRight = self.txtRight
        txtLen = self.term_width // 2 - 2
        txtLeft = txtLeft[-txtLen:]
        txtRight = txtRight[:txtLen]
        line = self.term_height // 2
        posX = 1
        if len(txtLeft) < txtLen:
            posX = txtLen - len(txtLeft) + 1
        if self.stdscr:
            self.stdscr.addstr(line, posX, 
                                 txtLeft, 
                                 curses.color_pair(1))
            self.stdscr.addstr(line, txtLen + 1, 
                                 txtRight,
                                 curses.color_pair(2))

    def infoChangedHandler(self,
                           timePast = '00:00',
                           speed    = '0 CPM',
                           errors   = '00.0 %'):
        self.info = '{0:5} / {1:7} / {2:7}'.format(
            timePast, 
            speed,
            errors)
        self.redraw()
        
    def print_info(self):
        posX = self.term_width // 2 - len(
            self.info) // 2
        posY = self.term_height // 4
        if posX > 0:
            self.stdscr.addstr(posY, posX, self.info)
            
    def stringsChangedHandler(self, strings=('','')):
        self.txtLeft, self.txtRight = strings
        self.redraw()

    def pauseChangedHandler(self, paused=None):
        pass #TODO

    def exitHandler(self):
        self.txtLeft, self.txtRight = (
            ' stop ', ' please')
        self.redraw()
        self._restore_term()

    def resize(self):
        self.term_height, self.term_width = self.stdscr.getmaxyx()
        self.stdscr.clear()
        self.redraw()

    def redraw(self):
        if self.stdscr:
            self.print_strings()
            self.print_info()
            self.stdscr.refresh()

    def input_handler(self, inputs):
        if inputs == 'resize':
            self.resize()
        else:
            self.extern_input_handler(inputs)

    def run(self, input_handler=None, 
            init_strings=('no','data')):
        try:
            self.txtLeft = init_strings[0]
            self.txtRight = init_strings[1] 
            self.stdscr = curses.initscr()
            curses.start_color()
            curses.noecho()
            curses.cbreak()
            curses.curs_set(0)
            self.stdscr.keypad(0)
            curses.init_pair(1, curses.COLOR_WHITE,
                             curses.COLOR_BLUE)
            curses.init_pair(2, curses.COLOR_BLUE,
                             curses.COLOR_WHITE)
            self.resize()
            
            self.extern_input_handler = input_handler
            
            self.stringsChangedHandler(init_strings)
            self.redraw()
            
            while True:
                inputs = get_input(self.stdscr)
                self.input_handler(inputs)
                self.stdscr.refresh()
        finally:
            self._restore_term()

    def _restore_term(self):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.curs_set(1)
        curses.endwin()

