import logging 

LOGFILE = 'punch.log'
ERRLOG  = open('punch.err.log','w')
logging.basicConfig(filename=LOGFILE, level=logging.DEBUG)

def log(message):
    logging.debug(message)
