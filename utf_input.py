from codecs import escape_decode
from curses import KEY_RESIZE
""" 
dirty hack for python curses to read
multibyte characters 
"""

def get_input(scr):
    def get_check_next_byte():
        c = scr.getch()
        if 128 <= c <= 191:
            return c
        else:
            raise UnicodeError

    def is_esc(c):
        if c == 27:
            scr.nodelay(1)
            c1 = scr.getch()
            c2 = scr.getch()
            scr.nodelay(0)
            return c1 == c2 == -1

    _bytes = []
    c = scr.getch()

    if c <= 127: # 1 byte
        if is_esc(c):
            return 'esc'
        elif c == 10:
            return 'enter'
        else:
            return chr(c)
    elif 194 <= c <= 223: # 2 bytes
        _bytes.append(c)
        _bytes.append(get_check_next_byte())
        buf = r''.join(
            [r'\{0}'.format(
                    hex(b)[1:]) for b in _bytes])
        buf = escape_decode(buf.encode('utf-8'))[0]
        buf = buf.decode('utf-8')
        return buf
    elif 224 <= c <= 239:# 3 bytes
        return 
    elif 240 <= c <= 244:# 4 bytes
        return
    elif c == KEY_RESIZE:
        return 'resize'

