import sys

import cursview
import sounds

from model.roller import Roller        
from punchlog import log, ERRLOG
from config import get_config


roller = Roller(config=get_config())
screen = cursview.Screen()

#def input_handler(inputs):
#    if inputs == 'esc':
#        roller._pause()
#    elif not roller.paused:
#        roller._check_input_n_roll_strings(inputs)
#    

roller.refreshInfo    += (screen.infoChangedHandler)

roller.stringsChanged += (screen.stringsChangedHandler)

roller.pauseChanged   += (screen.pauseChangedHandler)

roller.exitEvent      += (screen.exitHandler)

player = None

sys.stderr = ERRLOG

try:
    if True: #TODO config sound enabled
        player = sounds.Player()
        roller.errorOccurred +=  player.err_sound
        roller.stringsChanged += player.press_sound

    roller.start()
    strings = roller.get_strings()
    screen.run(input_handler=roller.input_handler,
                            init_strings=strings)
except KeyboardInterrupt:
    print(roller.get_stat_string())
    if player:
        player.stop()
    roller.stop()


