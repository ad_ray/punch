import time
import threading

import events
import model.data as data
from punchlog import log

NEWLINE = chr(182)

#import functools


class Roller:
    def __init__(self, config=None):
        self.strLeft        = 'Press space '
        self.strRight       = 'to continue'
        self.correctChars   = 0
        self.incorrectChars = 0
        self.paused         = False
        self.secsPast       = 0

 
        self.refreshInfo      = events.Event()
        self.stringsChanged   = events.Event()
        self.pauseChanged     = events.Event()
        self.errorOccurred    = events.Event()
        self.exitEvent        = events.Event()

        self.started = False

        self.lock    = threading.Lock()

        self.conf = config
        self.reader = data.readersFactory(config)
        self._set_strings()
        self.timerThread = threading.Thread(target=self._timer)
        self.timerThread.daemon = True

    def start(self):  
        self.started = True
        self.timerThread.start()

    def get_strings(self):
        return self.strLeft, self.strRight

    def get_stat_string(self):
        dat = self._get_stat()
        return "speed: {}, time: {}, errors: {}".format(
            dat.speed, dat.time, dat.errors)
    
    def get_speed(self):
        return str(self._get_speed_cpm()) + 'CPM'

    def stop(self):
        self._exit()

    def input_handler(self, inputs):
        if inputs == 'esc':
            self._pause()
        elif not self.paused:
            if self._is_input_char_correct(inputs):
                self._roll_strings()
            else:
                self._incorrect_input()

    def _get_formated_time(self):
        return '{0:02d}:{1:02d}'.format(
            round(self.secsPast) // 60,
            round(self.secsPast) % 60)

    def _timer(self):
        timeStart = time.time()
        while True:
            for i in range(10):
                time.sleep(0.1)
                if not self.paused:
                    self.secsPast += time.time() - timeStart
                timeStart = time.time()
            if not self.paused:
                self.refreshInfo.fire(
                    timePast = self._get_formated_time(),
                    errors   = self._get_error_percentage(),
                    speed    = self.get_speed())
            if self.secsPast > 60 * 99:
                break
        self._exit()
        
    def _exit(self):
        self._unsubscribe_events()
        self.exitEvent.fire()
        self._write_stat()
        del(self.timerThread)
        
    def _unsubscribe_events(self):
        self.refreshInfo.clean()
        self.stringsChanged.clean()
        self.pauseChanged.clean()
        self.errorOccurred.clean()

    def _pause(self):
        self.paused = not self.paused
        self.pauseChanged.fire(paused=self.paused)

    def _set_strings(self):
        self.strRight = self.reader.get_init_buffer()
        self.strLeft = ' ' * 64
        self.stringsChanged.fire(strings=self.get_strings())

    def _get_next_char(self):
        return self.reader.get_next_char()

    def _is_input_char_correct(self, input_c):
        return (input_c == self.strRight[0] or
                (input_c == 'enter' and
                 self.strRight[0] == NEWLINE))

    def _incorrect_input(self):
        self.incorrectChars += 1
        self.errorOccurred.fire()

    def _get_error_percentage(self):
        try:
            return str(round(
                self.incorrectChars
                / (self.incorrectChars + self.correctChars)
                * 100,
                1)) + '%'
        except ZeroDivisionError:
            return '00.0%'

    def _get_speed_cpm(self):
        return round(self.correctChars / self.secsPast * 60, 1)

    def _roll_strings(self):
        self.strLeft  = (self.strLeft[1:] + self.strRight[0])
        self.strRight = (self.strRight[1:]
                           + self.reader.get_next_char())
        self.correctChars += 1
        self.stringsChanged.fire(strings=self.get_strings())
    
    def _write_stat(self):
        if self.secsPast > self.conf['stat_time']:
            
            dat = self._get_stat()
            data.write_stats(data=dat, 
                                config=self.conf)

    def _get_stat(self):
        dat = lambda: None
        dat.speed  = self._get_speed_cpm()
        dat.time   = self._get_formated_time()
        dat.errors = float(self._get_error_percentage()[:-1])
        return dat
