import logging
from time import strftime
import random
#import string
import os
import errno

from punchlog import log

NEWLINE = chr(182)


class PhrasesReader:
    
    def __init__(self, filename=None, bufSize=64):
        self.bufSize = bufSize
        self.lines   = list(
          filter(lambda line: len(line) > 2, open(filename)))
        self.initBuffer = None
        self.buffer = None
        self.set_buffer_n_init_buffer()
        self.char = self.char_generator()

    def get_next_char(self):
        return next(self.char)

    def char_generator(self):
        while True:
            for c in self.buffer:
                yield c
            self.buffer = self.filter_string(
              self.get_random_line())
    
    def set_buffer_n_init_buffer(self):
        buf = ''
        while len(buf) < self.bufSize:
            buf += self.get_random_line()
        buf = self.filter_string(buf)
        self.initBuffer = buf[:self.bufSize]
        self.buffer     = buf[self.bufSize:]
      
    def filter_string(self, string):
        return string.replace('\n', NEWLINE)
    
    def get_init_buffer(self):
        return self.initBuffer

    def get_random_line(self):
        return self.lines[random.randint(0, len(self.lines) - 1)]
    
    
class Digits:
    space_digits = list(range(10))
    space_digits.append(' ')
    space_digits.append(' ')

    def __init__(self, bufSize=64):
        self.bufSize = bufSize

    def get_next_char(self):
        return str(random.choice(Digits.space_digits))

    def get_init_buffer(self):
        return ''.join(
            str(random.choice(Digits.space_digits)) 
            for i in range(0, self.bufSize))

    
def readersFactory(config):
    if config['mode'] == 'phrases':
        return PhrasesReader(filename=config['file'])
    elif config['mode'] == 'digits':
        return Digits()

def _mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def write_stats(config=None, data=None):
    path = config['stat_path'] + config['stat_dir']
    _mkdirs(path)
    fileName = '{0}{1} {2} {3:5.1f} {4:4.1f}'.format(
      path,
      strftime('%y.%m.%d %H:%M:%S'),
      data.time,
      data.speed,
      data.errors)
    open(fileName, mode='w')
